module.exports = {
  manifest: require('./manifest'),
  deployable: require('./deployable'),
  license: require('./license')
}
