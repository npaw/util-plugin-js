Object.assign = require('./assign') // Object.assign polyfill
var analyze = require('./analyze')
var readFile = require('./readFile')
var fs = require('fs')

var defaults = {
  package: './package.json',
  adapter: './src/adapter.js',
  adsAdapters: './src/ads/',
  output: './manifest.json',
  callback: null
}

/**
 * Creates a manifest file:
 * var defaults = {
 *   package: './package.json',
 *   adapter: './src/adapter.js',
 *   adsAdapters: './src/ads/',
 *   output: './manifest.json',
 *   callback: null
 * }
 *
 * @param {Obect} options
 */
module.exports = function (options) {
  var opt = Object.assign({}, defaults, options || {})

  // File reading
  var pkg = JSON.parse(readFile(opt.package))
  if (pkg) {
    var adapter = readFile(opt.adapter)
    var adsAdapters = readFile.readFolder(opt.adsAdapters)

    // Create manifest
    var manifest = {
      name: pkg.name,
      type: 'lib',
      tech: 'js',
      author: pkg.author,
      version: pkg.version,
      built: new Date().toJSON().slice(0, 10)
    }

    // Repo
    if (typeof pkg.repository === 'string') {
      manifest.repo = pkg.repository
    } else if (pkg.repository && pkg.repository.url) {
      manifest.repo = pkg.repository.url
    }
    if (manifest.repo.indexOf('git+') === 0) manifest.repo = manifest.repo.slice(4)

    // If adapter
    if (manifest.name && manifest.name.indexOf('youbora-adapter-') === 0) {
      manifest.name = pkg.name.slice(16)
      manifest.type = 'adapter'
      manifest.libVersion = pkg.dependencies.youboralib

      if (adapter) manifest.features = analyze(adapter)
      manifest.ads = []
      for (var filename in adsAdapters) {
        var content = adsAdapters[filename]
        console.log(filename)
        var info = analyze(content)
        info.filename = filename
        manifest.ads.push(info)
      }
    }

    fs.writeFile(opt.output, JSON.stringify(manifest, null, '  '), { mode: '664' }, function (err) {
      if (err) {
        console.error(err)
        process.exit(-1)
      }

      if (typeof opt.callback === 'function') opt.callback()
    })
  } else {
    console.error(opt.package + ' not found.')
    process.exit(-1)
  }
}
