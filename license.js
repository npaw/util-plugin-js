Object.assign = require('./assign') // Object.assign polyfill
var readFile = require('./readFile')

var defaults = {
  package: './package.json'
}

module.exports = function (options) {
  var opt = Object.assign({}, defaults, options || {})
  var pkg = opt.package
  if (typeof pkg === 'string') pkg = JSON.parse(readFile(pkg))

  return '@license ' + pkg.license +
    '\n' + pkg.name + ' ' + pkg.version +
    '\nCopyright NicePopleAtWork <http://nicepeopleatwork.com/>' +
    '\n@author ' + pkg.author +
    '\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.'
}
