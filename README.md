# util-plugins-js
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

This package contains certain utilities to plugin building and developing, can be used to build manifests and create deployable folders.

## Install
```
npm install --save-dev youboralib-util
```

## Usage

```javascript
var yuitl = require('youboralib-util')
yutil.manifest(options) // Will create a manifest.json file with source code analyzed
yutil.deployable(options) // Will create a manifest.json file with source code analyzed
```

You can also use the console:

```bash
$ ./node_modules/.bin/youboralib-util manifest
$ ./node_modules/.bin/youboralib-util deployable
```

Console commands will use default values.


### Manifest
`manifest` receives an options object, that can receive:

* `output`: Name of the output file. **Default:** 'manifest.json'.
* `package`: Path to the package.json file of the adapter. **Default:** './package.json'.
* `adapter`: Path to the adapter file. **Default:** './src/adapter.js'.
* `adsAdapters`: Path to ads adapters folder **Default:** './src/ads/'.
* `callback`: A function to be called when the script finishes. **Default:** null.

Generates a manifest.json file, analyzing the provided files.

### Deployable
`deployable` receives an options object, that can receive:

* `output`: Name of the output file. **Default:** './dist/'.
* `manifest`: Path to the manifest.json file of the adapter. **Default:** './manifest.json'.
* `copy`: Array of deployable files. **Default:** ['./manifest.json', './dist/\*\*/\*','./samples/\*\*/\*'].
* `prodFiles`: Array of production files. **Default:** ['./manifest.json', './dist/\*.js'].
* `callback`: A function to be called when the script finishes. **Default:** null.

Generates /deploy/ folder with files ready to deploy.

### License
`license` receives an options object, that can receive:

* `package`: Path to the package.json file or the parsed content of the file. **Default:** './package.json'.

Returns (or prints in console) the license banner for webpack.
