/**
 * This function analyzes an adapter and returns an object containing its findings.
 *
 * @param {String} file Contents of the file.
 * @returns {Object}
 */
module.exports = function (file) {
  var allgetters = ['getPlayhead', 'getPlayrate', 'getFramesPerSecond', 'getDroppedFrames',
    'getDuration', 'getBitrate', 'getThroughput', 'getRendition', 'getTitle', 'getTitle2',
    'getIsLive', 'getResource', 'getPlayerVersion', 'getPosition', 'getCdnTraffic', 'getP2PTraffic',
    'getUploadTraffic', 'getIsP2PEnabled', 'getHouseholdId', 'getLatency', 'getPacketLoss',
    'getPacketSent', 'getMetrics', 'getGivenBreaks', 'getExpectedBreaks', 'getExpectedPattern',
    'getBreaksTime', 'getGivenAds', 'getExpectedAds', 'getIsVisible', 'getAudioEnabled',
    'getIsSkippable', 'getIsFullscreen', 'getCampaign', 'getCreativeId', 'getProvider']

  var getters = []
  for (var i = 0; i < allgetters.length; i++) {
    var element = allgetters[i]
    if (file.indexOf(element) !== -1) {
      getters.push(element)
    }
  }

  return {
    buffer: file.indexOf('fireBufferBegin') !== -1 ? 'native' : 'monitor',
    seek: file.indexOf('fireSeekBegin') !== -1 ? 'native' : 'monitor',
    error: file.indexOf('fireError') !== -1 || file.indexOf('fireFatalError') !== -1,
    stop: file.indexOf('fireStop') !== -1,
    pause: file.indexOf('firePause') !== -1,
    resume: file.indexOf('fireResume') !== -1,
    getters: getters
  }
}
