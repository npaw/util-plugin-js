## [1.4.3] - 2023-05-10
### Added
- Add, PROD file, on root path too (to reference on documentation link)

## [1.4.2] - 2022-10-14
### Added
- Change, on deployable file, version path name

## [1.4.1] - 2020-02-24
### Added
- Copyright message

## [1.4.0] - 2019-09-04
### Added
- Player version getter
- Detection of events for manifest

## [1.3.4] - 2019-05-29
### Added
- New getters in adnalyzer

## [1.3.2] - 2018-07-30
### Added
- New getters in adnalyze
- Changed `deployable` defaults to include subfolders.

## [1.3.1] - 2017-06-20
### Changed
- Changed `deployable` defaults to include subfolders.

## [1.3.0]
### Added
- `deployable` will generate `prodfile` folder containing the PROD file.

## [1.2.1]
### Changed
- `deployable` does no longer create `dist` folder.

## [1.2.0]
### Added
- `deployable` will generate a `prod` folder containing the sp and the manifest.
- `prodFiles` options to `deployable`.

## [1.1.1]
### Changed
- Added `*.md` to `deployable`.

## [1.1.0]
### Added
- `license` feature.

### Changed
- Added console outputs for deployable and manifest when used in console.


## [1.0.0]
### Added
- `deployable` feature.
- `manifest` feature.
