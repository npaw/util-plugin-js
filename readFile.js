var fs = require('fs')

/**
 * Read defined file and return contents.
 *
 * @param {string} path File path
 * @returns {string} contents.
 */
module.exports = function (path) {
  return fs.existsSync(path) ? fs.readFileSync(path, 'utf8') : null
}

/**
 * Read each file in specified folder.
 *
 * @param {string} path Path to the folder. ie: './src/ads/'
 * @returns {Object} filename:content object with all the files inside said folder.
 */
module.exports.readFolder = function (path) {
  var ret = {}
  if (fs.existsSync(path)) {
    var files = fs.readdirSync(path)
    for (var i = 0; i < files.length; i++) {
      var filename = files[i]
      ret[filename] = fs.readFileSync(path + filename, 'utf8')
    }
  }

  return ret
}
