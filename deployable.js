Object.assign = require('./assign') // Object.assign polyfill
var copyfiles = require('copyfiles')
var readFile = require('./readFile')
var fs = require('fs')
var mkdirp = require('mkdirp')

var defaults = {
  manifest: './manifest.json',
  copy: [
    './manifest.json',
    './*.md',
    './dist/**/*',
    './samples/**/*'
  ],
  prodFiles: [
    './manifest.json',
    './dist/*.js'
  ],
  output: './deploy/',
  callback: null
}

/**
 * Copy files for deployment.
 *
 * var defaults = {
 *   manifest: './manifest.json',
 *   copy: [
 *     './manifest.json',
 *     './dist/*',
 *     './samples/*'
 *   ],
 *   prodFiles: [
 *     './manifest.json',
 *     './dist/*.js'
 *   ],
 *   output: './deploy/',
 *   callback: null
 * }
 *
 * @param {Object} options
 */
module.exports = function (options) {
  var opt = Object.assign({}, defaults, options || {})

  var manifest = JSON.parse(readFile(opt.manifest))
  if (manifest) {
    var dir = '/lib/'
    var file = 'npawlib.min.js'
    if (manifest.type === 'adapter') {
      dir = '/adapters/' + manifest.name + '/'
      file = 'sp.min.js'
    }
    var files
    copyLastBuild()
  } else {
    console.error(opt.manifest + ' not found.')
    process.exit(-1)
  }

  function copyLastBuild () {
    var files = opt.copy.slice(0) // clone
    files.push(opt.output + 'last-build' + dir + 'last-build')
    copyfiles(files, false, copyVersion)
  }

  function copyVersion (err) {
    if (err) console.error(err)

    files = opt.copy.slice(0) // clone
    files.push(opt.output + 'version' + dir + manifest.version)
    copyfiles(files, false, copyProd)
  }

  function copyProd (err) {
    if (err) console.error(err)

    files = opt.prodFiles.slice(0) // clone
    files.push(opt.output + 'prod' + dir + manifest.version)
    copyfiles(files, true, copyProdfile)
  }

  function copyProdfile (err) {
    if (err) console.error(err)
    var path = opt.output + 'prodfile' + dir + manifest.version
    var pathLastBuild = opt.output + 'prodfile' + dir
    mkdirp.sync(path)
    fs.writeFile(
      path + '/PROD',
      'https://smartplugin.youbora.com/v7/js' + dir + manifest.version + '/' + file,
      { mode: '664' },
      lastCallback
    )
    fs.writeFile(
      pathLastBuild + '/LAST-PROD',
      'https://smartplugin.youbora.com/v7/js' + dir + manifest.version + '/' + file,
      { mode: '664' },
      lastCallback
    )
  }

  function lastCallback (err) {
    if (err) console.error(err)

    if (typeof opt.callback === 'function') opt.callback()
  }
}
